﻿using api_net.Entites;
using api_net.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using api_net.Helpers;
using Microsoft.Extensions.Options;

namespace api_net.Controllers.Auth
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly AppSetting _appSettings;

        public LoginController(IOptionsMonitor<AppSetting> optionsMonitor)
        {
            _appSettings = optionsMonitor.CurrentValue;
        }

        [HttpPost]
        public IActionResult Login([FromBody] LoginModel request)
        {

            return Ok(new
            {
                Success = true,
                Message = "Authenticate success",
                Data = GenerateToken(request)
            });
        }

        private string GenerateToken(LoginModel request)
        {
            var jwtTokenHandler = new JwtSecurityTokenHandler();

            var secretKeyBytes = Encoding.UTF8.GetBytes(_appSettings.SecretKey);

            var tokenDescription = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] {
                    new Claim(ClaimTypes.Name, request.UserName),
                    new Claim("Password", request.Password),
                    new Claim("TokenId", Guid.NewGuid().ToString())
                }),
                //time live
                Expires = DateTime.UtcNow.AddMinutes(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(secretKeyBytes), SecurityAlgorithms.HmacSha512Signature)
            };

            var token = jwtTokenHandler.CreateToken(tokenDescription);

            return jwtTokenHandler.WriteToken(token);
        }
    }
}
