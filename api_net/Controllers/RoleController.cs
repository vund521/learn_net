﻿using api_net.Entites;
using api_net.Models;
using api_net.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace api_net.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly IRoleService _roleService;

        public RoleController(IRoleService roleService) {
            _roleService = roleService;
        }

        [HttpGet]
        public IActionResult GetAll() {
            try
            {
                return StatusCode(StatusCodes.Status200OK, _roleService.GetRoles());
            }
            catch (Exception ex)
            {

                return BadRequest(ex);
            }
        }

        [HttpPost]
        public IActionResult UpdateRole(RoleModel request)
        {
            try
            {
                _roleService.StoreRole(request);
                return StatusCode(StatusCodes.Status200OK);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status400BadRequest, ex);
            }
        }

        [HttpPut("{id}")]
        public IActionResult PutRole(int id, RoleModel request)
        {
            if (request == null)
            {
                return BadRequest();
            }

            try
            {
                _roleService.UpdateRole(id, request);
                return StatusCode(StatusCodes.Status200OK, new
                {
                    message = "Cập nhật thành công"
                });
            }
            catch (Exception ex)
            {

                return StatusCode(StatusCodes.Status400BadRequest, ex);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult actionResultDeleteRole(int id)
        {
            try
            {
                _roleService.DeleteRole(id);
                return StatusCode(StatusCodes.Status200OK, new
                {
                    message = "Xóa thành công"
                });
            }
            catch (Exception ex)
            {

                return StatusCode(StatusCodes.Status400BadRequest, ex);
            }
        }
    }
}
