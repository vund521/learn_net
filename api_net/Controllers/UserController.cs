﻿using api_net.Helpers;
using api_net.Models;
using api_net.Repositories.UserRepostiories;
using api_net.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace api_net.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [Authorize]
        [HttpGet]
        public IActionResult getAllUser([FromQuery] ParamsQueryData paramUrl)
        {
            var data = _userService.loadDataItems(paramUrl);
            return Ok(data);
        }

        [HttpGet("{id}")]
        public IActionResult findUserById(int id)
        {
            try
            {
                var infoUser = _userService.GetUserInfo(id);
                return Ok(infoUser);
            }
            catch (Exception ex)
            {
                return NotFound(ex);
            }
        }

        [HttpPost]
        public IActionResult store(UserModel request)
        {
            if (request == null)
            {
                return BadRequest();
            }

            try
            {
                _userService.Store(request);
                return StatusCode(StatusCodes.Status200OK, request);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpPut("{id}")]
        public IActionResult update(int id, UserModel resquest)
        {
            if (resquest == null)
            {
                return BadRequest();
            }

            try
            {
                _userService.Update(id, resquest);

                return Ok(new
                {
                    status = 1,
                    message = "Cập nhật thành công",
                    data = resquest
                });

            }
            catch (Exception ex)
            {
                return BadRequest($"{ex.Message}");
            }

        }

        [HttpDelete("{id}")]
        public IActionResult delete(int id)
        {
            try
            {
                _userService.Destroy(id);
                return StatusCode(StatusCodes.Status200OK, new
                {
                    message = "Xóa thành công",
                });
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);

            }
        }
    }
}
