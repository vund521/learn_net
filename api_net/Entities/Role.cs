﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace api_net.Entites
{
    public class Role
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public ICollection<RoleUser> RoleUsers { get; set; }

        public Role()
        {
            RoleUsers = new List<RoleUser>();
        }
    }
}
