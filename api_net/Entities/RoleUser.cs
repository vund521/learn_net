﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace api_net.Entites
{
    public class RoleUser
    {
        public int RoleId { get; set; }
        public Role Role { get; set; } // relationship

        public int UserId { get; set; }
        public User User { get; set; } // relationship

    }
}
