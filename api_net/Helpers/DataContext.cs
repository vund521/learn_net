﻿using api_net.Entites;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;

namespace api_net.Helpers
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions options) : base(options){}
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>(e =>
            {
                e.HasKey(e => e.Id);
            });

            modelBuilder.Entity<Role>(e =>
            {
                e.HasKey(e => e.Id);
            });

            modelBuilder.Entity<RoleUser>().HasKey(sc => new { sc.RoleId, sc.UserId});

            modelBuilder.Entity<RoleUser>()
                        .HasOne<User>(sc => sc.User)
                        .WithMany(s => s.RoleUsers)
                        .HasForeignKey(sc => sc.UserId);



            modelBuilder.Entity<RoleUser>()
                .HasOne<Role>(sc => sc.Role)
                .WithMany(s => s.RoleUsers)
                .HasForeignKey(sc => sc.RoleId);
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Job> Jobs { get; set; }
        public DbSet<RoleUser> RoleUsers { get; set; }
        public DbSet<Role> Roles { get; set; }
    }

}
