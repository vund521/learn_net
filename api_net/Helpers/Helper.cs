﻿
using api_net.Models;

namespace api_net.Helpers
{
    public class Helper<T>
    {
        public static PaginateModel<T> paginate(IEnumerable<T> source, int pageIndex, int pageSize)
        {
            List<T> items = source.Skip(pageIndex * pageSize).Take(pageSize).ToList();
            int count = source.Count();
            return new PaginateModel<T>
            {
                Data = items,
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalCount = count
            };
        }
    }
}
