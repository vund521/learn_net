﻿namespace api_net.Models
{
    public class PaginateModel<T>
    {
        public List<T> Data { get; set; }
        public int PageIndex { get; set; }
        public int TotalCount { get; set; }
        public int  PageSize { get; set; }
    }
}
