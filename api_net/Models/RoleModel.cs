﻿using System.ComponentModel.DataAnnotations;

namespace api_net.Models
{
    public class RoleModel
    {
        [Required]
        public string Name { get; set; }
    }
}
