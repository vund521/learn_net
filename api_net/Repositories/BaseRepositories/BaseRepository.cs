﻿using api_net.Helpers;

namespace api_net.Repositories.UserRepostiories
{
    public class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        protected readonly DataContext _content;
        public BaseRepository(DataContext context)
        {
            _content = context;
        }

        public void Delete(T entity)
        {
            _content.Set<T>().Remove(entity);
            _content.SaveChanges();
        }

        public IEnumerable<T> GetAll(ParamsQueryData? param = null)
        {
            return _content.Set<T>().AsQueryable();
            //if (param != null)
            //{
            
            //    allData.Skip(param.page).Take(param.pageSize);
            //}
            
            //return allData.ToList();
        }

        public T GetInfoById(int id)
        {
            return _content.Set<T>().Find(id);
        }

        public void Store(T entity)
        {
           _content.Set<T>().Add(entity);
           _content.SaveChanges();
        }

        public void Update(T entity)
        {
            _content.Set<T>().Update(entity);
            _content.SaveChanges();
        }
    }
}
