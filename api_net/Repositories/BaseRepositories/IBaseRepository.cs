﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace api_net.Repositories.UserRepostiories
{
    public interface IBaseRepository<T> where T : class
    {
        T GetInfoById(int id);
        IEnumerable<T> GetAll(ParamsQueryData? param = null);
        
        void Store(T entity);

        void Update(T entity);

        void Delete(T entity);

    }
    public class ParamsQueryData
    {
        public string? search { get; set; } = null;
        public int page { get; set; }
        public int pageSize { get; set; }
    }
}
