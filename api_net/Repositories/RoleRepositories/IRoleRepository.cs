﻿using api_net.Entites;
using api_net.Repositories.UserRepostiories;

namespace api_net.Repositories.RoleRepositories
{
    public interface IRoleRepository: IBaseRepository<Role>
    {
    }
}
