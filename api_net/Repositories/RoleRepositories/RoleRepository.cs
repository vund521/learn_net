﻿using api_net.Entites;
using api_net.Helpers;
using api_net.Repositories.UserRepostiories;

namespace api_net.Repositories.RoleRepositories
{
    public class RoleRepository : BaseRepository<Role>, IRoleRepository
    {
        public RoleRepository(DataContext context) : base(context)
        {
        }
    }
}
