﻿using api_net.Entites;
using api_net.Models;
using api_net.Repositories.RoleRepositories;

namespace api_net.Services
{
    public class RoleService : IRoleService
    {
        private readonly IRoleRepository _roleRepository;
        public RoleService(IRoleRepository roleRepository) {
            _roleRepository = roleRepository;
        }

        public IEnumerable<Role> GetRoles()
        {
            return _roleRepository.GetAll();
        }

        public Role GetRoleById(int id)
        {
            return _roleRepository.GetInfoById(id);
        }

        public void UpdateRole(int id, RoleModel role)
        {
            Role dataUpload = GetRoleById(id);
            dataUpload.Name = role.Name;

            _roleRepository.Update(dataUpload);
        }

        public void StoreRole(RoleModel request)
        {
            Role dataUpload = new Role
            {
                Name = request.Name,
            };

            _roleRepository.Store(dataUpload);
        }

        public void DeleteRole(int id)
        {
            Role role = GetRoleById(id);
            _roleRepository.Delete(role);
        }
    }

    public interface IRoleService
    {
        IEnumerable<Role> GetRoles();
        Role GetRoleById(int id);
        void StoreRole(RoleModel request);
        void UpdateRole(int id, RoleModel request);

        void DeleteRole(int id);
    }

}
