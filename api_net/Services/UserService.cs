﻿using api_net.Entites;
using api_net.Helpers;
using api_net.Models;
using api_net.Repositories.UserRepostiories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.Collections;

namespace api_net.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public PaginateModel<User> loadDataItems(ParamsQueryData paramUrl)
        {
            var dataCollections = _userRepository.GetAll();
            if (!paramUrl.search.IsNullOrEmpty())
            {
                string keyword = paramUrl.search;
                dataCollections = dataCollections.Where(item => item.Title.Contains(keyword));
            }

            //ResultDataPaginateModel result = dataCollections

            return Helper<User>.paginate(dataCollections, paramUrl.page, paramUrl.pageSize);
        }

        public User GetUserInfo(int Id)
        {
            return _userRepository.GetInfoById(Id);
        }

        public void Store(UserModel request)
        {
            User dataUpload = new User
            {
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Password = request.Password,
                Role = request.Role,
                Title = request.Title,
            };

            _userRepository.Store(dataUpload);

        }

        public void Update(int id, UserModel request)
        {
            User user = _userRepository.GetInfoById(id);
            user.FirstName = request.FirstName;
            user.LastName = request.LastName;
            user.Password = request.Password;
            user.Role = request.Role;
            user.Title = request.Title;
            user.Email = request.Email;
            _userRepository.Update(user);
        }

        public void Destroy(int id)
        {
            User user = _userRepository.GetInfoById(id);
            _userRepository.Delete(user);
        }
    }

    public interface IUserService
    {
        PaginateModel<User> loadDataItems(ParamsQueryData paramUrl);
        User GetUserInfo(int Id);
        public void Store(UserModel dataUpload);
        public void Update(int id, UserModel user);
        public void Destroy(int id);
    }
}

